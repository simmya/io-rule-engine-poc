package com.indegene.rule.engine.ruleengine;

import com.indegene.rule.engine.ruleengine.drools.config.DroolsConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

//@Import(DroolsConfiguration.class)
@SpringBootApplication
public class RuleEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuleEngineApplication.class, args);
	}
}
