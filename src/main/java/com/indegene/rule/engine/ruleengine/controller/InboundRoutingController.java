package com.indegene.rule.engine.ruleengine.controller;

import com.indegene.rule.engine.ruleengine.model.AvailableReps;
import com.indegene.rule.engine.ruleengine.model.Profiles;
import com.indegene.rule.engine.ruleengine.model.RepProfile;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simmya on 8/9/2017.
 */
@RestController
public class InboundRoutingController {

    @Autowired
    KieSession kieSession;

    @GetMapping(value="/test")
    public ResponseEntity<?> getTestData(@RequestParam("id") String id){
        return ResponseEntity.ok("test");
    }

    @PostMapping(value="/test/post")
    public ResponseEntity<?> postTestData(String test){
        return ResponseEntity.ok("Test");
    }


    @PostMapping(value = "/reps/available/rule/1")
    public ResponseEntity<?> getPriorityReps(@RequestBody Profiles repProfiles){
        AvailableReps availableReps = new AvailableReps();
        FactHandle repHandle = kieSession.insert(repProfiles);
        FactHandle availableRepHandle = kieSession.insert(availableReps);
        kieSession.fireAllRules();
        kieSession.retract(repHandle);
        kieSession.retract(availableRepHandle);
        System.out.println("Available reps >> "+availableReps.getRepIds());
        return ResponseEntity.ok(availableReps);
    }

    @PostMapping(value = "/reps/available/rule/2")
    public ResponseEntity<?> getPriorityRepWithMaxWaitPeriod(@RequestBody Profiles repProfiles){
        AvailableReps availableReps = new AvailableReps();
        Boolean rule_2_flag = true;

        FactHandle rule2FlagHandle = kieSession.insert(rule_2_flag);
        FactHandle repHandle = kieSession.insert(repProfiles);
        FactHandle availableRepHandle = kieSession.insert(availableReps);

        kieSession.fireAllRules();
        kieSession.retract(rule2FlagHandle);
        kieSession.retract(availableRepHandle);
        kieSession.retract(repHandle);
        return ResponseEntity.ok(availableReps);
    }

    @PostMapping(value = "/reps/available/rule/remote")
    public ResponseEntity<?> executeRuleFromRemoteURL(@RequestBody Profiles repProfiles){
        KieSession kieSession = getKieSessionForRemoteURL();
        AvailableReps availableReps = new AvailableReps();
        kieSession.insert(repProfiles);
        kieSession.insert(availableReps);
        kieSession.fireAllRules();
        return ResponseEntity.ok(availableReps);
    }

    private KieSession getKieSessionForRemoteURL() {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kfs = kieServices.newKieFileSystem();
        kfs.write(ResourceFactory.newUrlResource("https://indegene.jfrog.io/indegene/list/libs-release-local/inbound-rep-list-available.drl"));
        KieBuilder kieBuilder = kieServices.newKieBuilder( kfs ).buildAll();
        KieContainer kieContainer =
                kieServices.newKieContainer( kieServices.getRepository().getDefaultReleaseId() );
        KieBase kieBase = kieContainer.getKieBase();
        return kieContainer.newKieSession();
    }


}
