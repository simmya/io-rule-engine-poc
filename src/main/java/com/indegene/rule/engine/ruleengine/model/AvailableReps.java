package com.indegene.rule.engine.ruleengine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;

/**
 * Created by simmya on 8/9/2017.
 */
public class AvailableReps {

    private Set<String> repIds = new HashSet<>();

    @JsonIgnore
    private HashMap<String, Integer> statusWaitMap = new HashMap<>();


    public Set<String> getRepIds() {
        return repIds;
    }

    public void setRepIds(Set<String> repIds) {
        this.repIds = repIds;
    }

    public HashMap getStatusWaitMap() {
        return statusWaitMap;
    }

    public void setStatusWaitMap(HashMap statusWaitMap) {
        this.statusWaitMap = statusWaitMap;
    }
}
