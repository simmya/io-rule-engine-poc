package com.indegene.rule.engine.ruleengine.model;

import java.util.List;

/**
 * Created by simmya on 8/9/2017.
 */
public class Profiles {

    public List<RepProfile> reps;

    public List<RepProfile> getReps() {
        return reps;
    }

    public void setReps(List<RepProfile> reps) {
        this.reps = reps;
    }
}
