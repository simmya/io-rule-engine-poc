package com.indegene.rule.engine.ruleengine.model;

/**
 * Created by simmya on 8/9/2017.
 */
public class RepProfile {
    public String name;
    public String ssn;
    public String type;
    public String teritory;
    public String brand;
    public String country;
    public String location;
    public String language;
    public String city;
    public String repId;
    public String status;
    public boolean filter;
    public int statusLastUpdatedIn;

    public int getStatusLastUpdatedIn() {
        return statusLastUpdatedIn;
    }

    public void setStatusLastUpdatedIn(int statusLastUpdatedIn) {
        this.statusLastUpdatedIn = statusLastUpdatedIn;
    }

    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTeritory() {
        return teritory;
    }

    public void setTeritory(String teritory) {
        this.teritory = teritory;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }
}
