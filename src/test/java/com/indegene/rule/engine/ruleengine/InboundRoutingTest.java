package com.indegene.rule.engine.ruleengine;

import com.indegene.rule.engine.ruleengine.model.AvailableReps;
import com.indegene.rule.engine.ruleengine.model.Profiles;
import com.indegene.rule.engine.ruleengine.model.RepProfile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.ObjectFilter;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simmya on 8/9/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RuleEngineApplication.class)
public class InboundRoutingTest {

    @Autowired
    private KieSession kieSession;

    @Test
    public void repProfileMatchedInboundRuleFirstPriority() {
        // Given
        RepProfile repProfile2 = new RepProfile();
        repProfile2.setRepId("123");
        repProfile2.setStatus("AVAILABLE");
        repProfile2.setCountry("CAN");

        RepProfile repProfile3 = new RepProfile();
        repProfile3.setRepId("124");
        repProfile3.setStatus("AVAILABLE");
        repProfile3.setCountry("ON");

        AvailableReps availableReps = new AvailableReps();
        kieSession.insert(repProfile2);
        kieSession.insert(repProfile3);
        kieSession.insert(availableReps);
        // }
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("Rules fired >>> "+ruleFiredCount);
        System.out.println("Available reps >> "+availableReps.getRepIds());

    }
    @Test
    public void repProfileMatchedInboundRuleSecondPriority() {
        // Given
        RepProfile repProfile2 = new RepProfile();
        repProfile2.setRepId("125");
        repProfile2.setStatus("AVAILABLE");
        repProfile2.setCountry("ON");

        RepProfile repProfile3 = new RepProfile();
        repProfile3.setRepId("126");
        repProfile3.setStatus("AVAILABLE");
        repProfile3.setCountry("ON");

        AvailableReps availableReps = new AvailableReps();
        kieSession.insert(repProfile2);
        kieSession.insert(repProfile3);
        kieSession.insert(availableReps);
        // }
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("Rules fired >>> "+ruleFiredCount);
        System.out.println("Available reps >> "+availableReps.getRepIds());

    }

    @Test
    public void repProfileRemoteURLRuleList() {
        // Given
        RepProfile repProfile2 = new RepProfile();
        repProfile2.setRepId("123");
        repProfile2.setStatus("AVAILABLE");
        repProfile2.setCountry("CAN");

        RepProfile repProfile3 = new RepProfile();
        repProfile3.setRepId("124");
        repProfile3.setStatus("AVAILABLEE");
        repProfile3.setCountry("ON");

        Profiles profiles = new Profiles();
        List<RepProfile> repProfiles = new ArrayList<>();
        repProfiles.add(repProfile2);
        repProfiles.add(repProfile3);
        profiles.setReps(repProfiles);

        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kfs = kieServices.newKieFileSystem();
       /* File file = new File("D:\\io_metadata\\drools\\rules\\inbound-rep-list-available.drl");
        kfs.write(ResourceFactory.newFileResource(file));*/
        kfs.write(ResourceFactory.newUrlResource("https://indegene.jfrog.io/indegene/list/libs-release-local/inbound-rep-list-available.drl"));
        KieBuilder kieBuilder = kieServices.newKieBuilder( kfs ).buildAll();
        KieContainer kieContainer =
                kieServices.newKieContainer( kieServices.getRepository().getDefaultReleaseId() );
        KieBase kieBase = kieContainer.getKieBase();
        KieSession kieSession = kieContainer.newKieSession();


        AvailableReps availableReps = new AvailableReps();
        kieSession.insert(profiles);
        kieSession.insert(availableReps);
        // }
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("Rules fired >>> "+ruleFiredCount);
        System.out.println("Available reps >> "+availableReps.getRepIds());


    }


    @Test
    public void repProfileRuleList() {
        // Given
        RepProfile repProfile2 = new RepProfile();
        repProfile2.setRepId("123");
        repProfile2.setStatus("AVAILABLE");
        repProfile2.setCountry("ON");

        RepProfile repProfile3 = new RepProfile();
        repProfile3.setRepId("124");
        repProfile3.setStatus("AVAILABLE");
        repProfile3.setCountry("ON");

        Profiles profiles = new Profiles();
        List<RepProfile> repProfiles = new ArrayList<>();
        repProfiles.add(repProfile2);
        repProfiles.add(repProfile3);
        profiles.setReps(repProfiles);


        AvailableReps availableReps = new AvailableReps();
        kieSession.insert(profiles);
        kieSession.insert(availableReps);
        int ruleFiredCount = kieSession.fireAllRules();
        System.out.println("Rules fired >>> "+ruleFiredCount);
        System.out.println("Available reps >> "+availableReps.getRepIds());


    }
}
